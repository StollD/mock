package main

import (
	"io"
	"net/http"
	"os"
	"os/user"
	"path"
	"strings"

	"golang.org/x/crypto/ssh/terminal"
)

/*
  The package for the two subsections of the application file
*/
type Package struct {
	Desktop *DesktopEntry
	App     *Application
}

/*
  Contains definitions from the freedesktop section of
  the application file we might want to access.
*/
type DesktopEntry struct {
	Icon string
}

/*
  Information about the application that is being started.
*/
type Application struct {

	// The docker image to run
	Image string

	// Directories that should get mounted, additionally to
	// the defaults for syncing users, sound, display etc.
	Mounts []string

	// Whether to pass through the X11 server to the container
	Display bool

	// Whether to pass through the pulseaudio daemon to the container
	Audio bool

	// Whether to run the container on the hosts network or a subnet
	Network bool

	// Whether to remove the container after it stopped
	Remove bool

	// Whether to mount system theme directories
	Themes bool

	// A link to a docker seccomp policy file that will be used
	Seccomp string

	// The maximum amount of memory the application can use
	Memory string

	// Additional docker arguments
	Arguments []string
}

/*
  Starts a docker container and uses X11 passthrough to
  display the contained application.
*/
func Run(app Package) {

	// Fetch user information
	usr, err := user.Current()
	if err != nil {
		panic(err)
		return
	}

	cmd := NewCommand("docker").
		Argument("run").
		Argument("-i").
		Parameter("user", usr.Uid+":"+usr.Gid).
		Parameter("env", "PARAMS="+strings.Join(os.Args[2:], " ")).
		Parameter("workdir", usr.HomeDir).
		Parameter("volume", usr.HomeDir+":"+usr.HomeDir).
		Parameter("volume", "/etc/group:/etc/group:ro").
		Parameter("volume", "/etc/passwd:/etc/passwd:ro").
		Parameter("volume", "/etc/shadow:/etc/shadow:ro").
		// TODO: Also add /etc/sudoers?
		Parameter("volume", "/etc/sudoers.d:/etc/sudoers.d:ro").
		Parameter("volume", "/etc/machine-id:/etc/machine-id:ro").
		Parameter("volume", "/dev/shm:/dev/shm").
		Parameter("volume", "/var/lib/dbus:/var/lib/dbus").
		Parameter("volume", "/var/run/dbus:/var/run/dbus").
		Parameter("volume", "/etc/localtime:/etc/localtime:ro").
		Parameter("volume", "/etc/hosts:/etc/hosts:ro").
		Parameter("device", "/dev/dri").
		Parameter("group-add", "video")

	if terminal.IsTerminal(int(os.Stdout.Fd())) {
		cmd = cmd.Argument("-t")
	}

	if app.App.Memory != "" {
		cmd = cmd.Parameter("memory", app.App.Memory)
	}

	if app.App.Remove {
		cmd = cmd.Argument("--rm")
	}

	if app.App.Display {
		cmd = cmd.Parameter("volume", "/tmp/.X11-unix:/tmp/.X11-unix:rw").
			Parameter("env", "DISPLAY")
	}

	if app.App.Themes {
		cmd = cmd.Parameter("volume", "/usr/share/themes:/usr/share/themes:ro").
			Parameter("volume", "/usr/share/icons:/usr/share/icons:ro").
			Parameter("volume", "/usr/share/fonts:/usr/share/fonts:ro")
	}

	if app.App.Audio {
		cmd = cmd.Parameter("device", "/dev/snd").
			Parameter("group-add", "audio").
			Parameter("volume", "/run/user/"+usr.Uid+"/pulse:/run/user/"+usr.Uid+"/pulse")
	}

	if app.App.Network {
		cmd = cmd.Argument("--net=host")
	}

	for _, mount := range app.App.Mounts {
		cmd = cmd.Parameter("volume", mount)
	}

	// Check for the seccomp profile
	if app.App.Seccomp != "" {
		cacheDir := path.Join(usr.HomeDir, ".cache", "mock", app.App.Image)
		cacheFile := path.Join(cacheDir, "seccomp.json")
		if _, err := os.Stat(cacheDir); os.IsNotExist(err) {
			err = os.MkdirAll(cacheDir, 0755)
			if err != nil {
				panic(err)
				return
			}
		}
		if _, err := os.Stat(cacheFile); os.IsNotExist(err) {
			err = DownloadFile(cacheFile, app.App.Seccomp)
			if err != nil {
				panic(err)
				return
			}
		}
		cmd = cmd.Parameter("security-opt", "seccomp="+cacheFile)
	}

	// Add additional arguments
	for _,arg := range app.App.Arguments {
		cmd = cmd.Argument(arg)
	}

	// Add the image
	cmd = cmd.Argument(app.App.Image)
	err = cmd.Run()
	if err != nil {
		panic(err)
	}
}

/*
  DownloadFile will download a url to a local file. It's efficient because it will
  write as it downloads and not load the whole file into memory.
  Source: https://golangcode.com/download-a-file-from-a-url/
*/
func DownloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}
