# Mock

Mock is a toolkit that allows you to easily run graphical end-user applications inside of a docker container. 
This allows for easy installation and uninstallation, while at the same time isolates each application in it's
own dedicated workspace. Users can only access their home directory that is passed through from the host. Every other
filesystem access is regulated through docker mounts by the system or network operator.

## How it works
Mock has two integral parts, the "frontend", which is an extension of the FreeDesktop .desktop files + a small utility 
application, as well as the "backend" which is a set of docker images + the docker daemon.

If you don't know about .desktop files, they are a standarized way to launch applications from start menus, the desktop
or a file explorer across all linux desktop environments. They are based on the INI file format, so they are extendable
and parseable using multiple libraries.

For mock applications this gets combined with the "shebang", a way of executing a shell script with a predefined shell.
If a shell script start with `#!/bin/bash`, and you run it this way `./script.sh`, the shell you run it in parses the 
first line, detects `/bin/bash` and then runs it with the script as the first parameter, like this: `/bin/bash ./script.sh`

But the shebang doesn't have to be a shell executable, it can be any program that somehow processes the attached file. 
For example, if you take a random shell script, and replace the shebang with `/bin/cat`, it would output the entire script
file into stdout if you run it. 

Usually .desktop files are not executed directly but instead interpreted by the desktop environment. Mock applications
have their .desktop files set as executable, with the line that is parsed by the DE and usually executes a program
executing the desktop file itself. **It is important that the Mock applications are stored within your PATH for this to 
work!** Their shebang points to the mock parser application that can now read the file and apply it's settings to a 
docker container.

This results in an environment, where installing is as easy as putting a .desktop file into your path, and enabling
it by symlinking it into `.local/share/applications`.

## Paths
* `/usr/mockapps` - Path where the .desktop files are stored, without file extensions
* `~/.local/share/applications` - Enabled applications get symlinked here, including .desktop file extension
* `/usr/bin/mock` - Mock utility application

## Installation
##### Dependencies
```bash
$ sudo pacman -S go docker
$ sudo usermod -aG docker $USER
$ sudo systemctl enable docker
$ sudo mkdir /usr/mockapps
$ echo 'export PATH=$PATH:/usr/mockapps' >> ~/.bashrc
$ sudo reboot
# probably similar for debian based systems
```
##### Build
```bash
$ git clone https://gitlab.com/StollD/mock
$ cd mock
$ go build
$ sudo cp ./mock /usr/bin
```

##### Docker images
```bash
$ cd mock
$ cd images

# build the base image
$ cd mock-base
$ docker build -t tmsp/mock-base .
$ cd ..

# build other images
$ cd chromium
$ docker build -t tmsp/chromium .
$ cd ..
```

##### Application
```bash
$ cd mock
$ cd apps
$ chmod +x chromium
$ sudo cp chromium /usr/mockapps/chromium
$ ln -s /usr/mockapps/chromium ~/.local/share/applications/chromium.desktop
```

Given that `chromium` isn't already installed on your system, the mockapp can now start it through the specified docker
image. The only real change to your system is to install and enable the definition for the mockapp. Everything else 
happens in an isolated container.

At some point a custom docker registry should be created to contain the images. Alternatively we could just use
docker hub directly.

## Mock Application definition
A normal desktop file looks like this:

```ini
[Desktop Entry]
Version=1.0
Name=MyApplication
Exec=/opt/myapp
Comment=Do things
Type=Application
Categories=GTK;Development;
Icon=myapp
```
(opt usually doesn't lie within path, thats why I am using it here. If myapp would already be in path, the change to the
`Exec` line wouldn't be neccessary)

Now, if we would transform this into a mock application, there are some things we have to do

##### Add a shebang:
```diff
+#!/usr/bin/env mock
+
 [Desktop Entry]
 Version=1.0
 Name=MyApplication
```

##### Make the file execute itself:
```diff
 [Desktop Entry]
 Version=1.0
 Name=MyApplication
-Exec=/usr/bin/myapp
+Exec=myapp
 Comment=Do things
 Type=Application
 Categories=GTK;Development;
 ```
 
##### Add the application definition:
```diff
 Type=Application
 Categories=GTK;Development;
 Icon=myapp
+
+[Application]
+Image=tmsp/myapp
+Display=true
+Audio=true
+Network=true
+Remove=true
+Themes=true
```

* `Image` - This is the docker image that will be pulled and used to run the application
* `Display` - Whether to pass through X11 output
* `Audio` - Whether to pass through audio devices
* `Network` - Whether to use the host network adapter inside the container
* `Remove` - Whether to remove the container after it has stopped
* `Themes` - Whether to pass through system wide theme directories, namely
    * `/usr/share/themes`
    * `/usr/share/icons`
    * `/usr/share/fonts`
* `Seccomp` - Link to a docker seccomp file that allows the container to access certain syscalls.

## Kernel level requirements
For some applications, like Chromium, it is required that your Kernel has been compiled with **User Namespaces** enabled.
Furthermore it is important that your system actually allows using them. Otherwise certain sandboxing features will fail
in those applications (which would exclude most if not all browsers except Firefox as they are mostly based on Chromium).

Run this to find out if your kernel was compiled with User Namespace support:
```bash
$ zgrep USER_NS /proc/config.gz
```
Kernels who are confirmed to have this enabled:
* Arch

If it returned `n` you will have to compile your own kernel. 
If it returned `y`, you will have to enable it on your system:
```bash
$ echo 'kernel.unprivileged_userns_clone=1' | sudo tee /etc/sysctl.d/00-local-userns.conf

# Either reboot
$ sudo reboot

# Or apply the setting for the running session
$ sudo sysctl kernel.unprivileged_userns_clone=1
```