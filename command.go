package main

import (
	"os"
	"os/exec"
)

type Command struct {
	params []string
	cmd    string
}

func NewCommand(cmd string) *Command {
	return &Command{cmd: cmd}
}

func (cmd *Command) Parameter(name string, value string) *Command {
	cmd.params = append(cmd.params, "--"+name+"="+value)
	return cmd
}

func (cmd *Command) Argument(name string) *Command {
	cmd.params = append(cmd.params, name)
	return cmd
}

func (cmd *Command) Run() error {
	ecmd := exec.Command(cmd.cmd, cmd.params...)
	ecmd.Stdout = os.Stdout
	ecmd.Stderr = os.Stderr
	ecmd.Stdin = os.Stdin
	return ecmd.Run()
}
