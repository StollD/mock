package main

import (
	"fmt"
	"github.com/shirou/gopsutil/process"
	"gopkg.in/ini.v1"
	"os"
	"os/user"
)

/*
  The entrypoint of the program. Check for errors in the arguments
  and load the passed application file.
*/
func main() {

	// Check if the program was launched with a commandline argument
	if len(os.Args) > 1 {

		file := os.Args[1]

		// Check if the file exists
		if _, err := os.Stat(file); os.IsNotExist(err) {
			fmt.Println("The file \"" + file + "\" does not exist!")
			os.Exit(1)
		}

		// Check if the docker daemon is running
		dockerRunning := false
		procs, err := process.Processes()
		if err != nil {
			panic(err)
			return
		}
		for _,proc := range procs {
			name, err := proc.Name()
			if err != nil {
				continue
			}
			if name == "dockerd" {
				dockerRunning = true
			}
		}
		if !dockerRunning {
			fmt.Println("The docker daemon is not running! Please start it through your distributions service manager!")
			os.Exit(2)
		}

		// Check if we are allowed to interact with the daemon
		usr, err := user.Current()
		if err != nil {
			panic(err)
			return
		}
		gids, err := usr.GroupIds()
		if err != nil {
			panic(err)
			return
		}
		dockerGroup, err := user.LookupGroup("docker")
		if err != nil {
			panic(err)
			return
		}
		dockerID := dockerGroup.Gid
		userAllowed := false
		for _,id := range gids {
			if id == dockerID {
				userAllowed = true
			}
		}
		if !userAllowed {
			fmt.Println("You are not allowed to interact with the docker daemon! Your user needs to be added to the docker groud!")
			os.Exit(3)
		}

		// Load and parse the file
		HandleFile(file)

	} else {

		// Output the help dialog
		PrintHelp()
	}
}

/*
  Parses the passed application file
*/
func HandleFile(file string) {

	// Create the wrapper
	app := Package{}
	app.Desktop = &DesktopEntry{}
	app.App = &Application{Remove: true, Themes: true, Network: true}

	// Load the ini files
	cfg, err := ini.Load(file)
	if err != nil {
		panic(err)
		return
	}
	err = cfg.Section("Desktop Entry").MapTo(app.Desktop)
	if err != nil {
		panic(err)
		return
	}
	err = cfg.Section("Application").MapTo(app.App)
	if err != nil {
		panic(err)
		return
	}

	// Run the specified application
	Run(app)
}

/*
  Outputs the help message to the commandline
*/
func PrintHelp() {
	fmt.Println("mock - Modular self-contained desktop applications using the docker framework")
	fmt.Println("Usage: mock <app definition file>")
}
