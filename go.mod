module gitlab.com/StollD/mock

require (
	github.com/shirou/gopsutil v0.0.0-20190131151121-071446942108
	golang.org/x/crypto v0.0.0-20190131182504-b8fe1690c613
	gopkg.in/ini.v1 v1.41.0
)
